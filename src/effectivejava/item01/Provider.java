package effectivejava.item01;

public interface Provider {
	Service newService();
}
